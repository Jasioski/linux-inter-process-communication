#include <stdio.h>
#include "ipcKeys.h"
#include <sys/ipc.h>
#include <sys/types.h>
#include <sys/msg.h>
#include <string.h>

/*
 * Message buffer struct that is used to pass messages around to other processes.
 */
struct msgbuf {
   long mtype;       /* message type, must be > 0 */
   char mtext[128];    /* message data */
};

/*
 * Enum to return a status of the program.
 * Sent by auxiliary methods and then used by the main
 * method to verify the health of the running process.
 */
enum status {
    SUCCESS, ERROR
};

/*
 * Generic send method function used to send messages to other processes.
 * based on a message queue ID (msqid).
 */
enum status sendMessage(int msqid, struct msgbuf sbuf) {
        // we are not using this type but this could be used to distinguish
        // different messages for example an acknowledge message vs data message
        sbuf.mtype = 1;

        // Send a message.
        size_t buf_length = strlen(sbuf.mtext) + 1;
        if((msgsnd(msqid, &sbuf, buf_length, IPC_NOWAIT)) < 0){
            printf("%d, %ld, %s, %zu\n", msqid, sbuf.mtype, sbuf.mtext, buf_length);
            perror("msgsnd");
            return ERROR;
        }
        else {
            printf("Message: \"%s\" Sent\n", sbuf.mtext);
        }
    return SUCCESS;
}

/*
 * Main method that will be executed when process is run.
 * Holds logic for getting user input and calling the message send function.
 */
int main() {
    printf("Process 1 Start\n");

    //create message queue for sending messages to process2
    int msqid = msgget(MSG_KEY, IPC_CREAT | 0666);
    struct msgbuf sbuf;

    if (msqid < 0) {
        perror("Msgget initialization failed");
        return -1;
    }

    char user_input[99];

    while (1) {
        printf("Please enter what to send to process 2: ");
        scanf("%s", user_input);
        printf("User Input: %s\n", user_input);
        printf("Sending Message to Process 2\n");
        strcpy(sbuf.mtext, user_input); //copy user input into msg buffer

        //send message to process 2
        if (sendMessage(msqid, sbuf) != SUCCESS) {
            printf("error when sending message");
            return -1;
        }
    }

    return 0;
}
