# linux-inter-process-communication

## Description
This will be an example of using linux IPC calls like message queues and shared memory to get
multiple processes (with different executables) to be able to communicate with each other.

The C programming language will be used to demonstrate these system calls.

## Instructions
1. Run `make`
2. Once make is completed run `process1`
3. Open another terminal window and run `process2`
4. Enter a message in the terminal window running `process1`
5. Observe that message being displayed in the terminal window running `process2`

## TODOS
- [x] Add message queues to send messages
- [ ] Add shared memory (and figure out how to demonstrate its use)
