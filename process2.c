#include <stdio.h>
#include "ipcKeys.h"
#include <sys/ipc.h>
#include <sys/types.h>
#include <sys/msg.h>

/*
 * Message buffer struct that is used to pass messages around to other processes.
 */
struct msgbuf {
   long mtype;       /* message type, must be > 0 */
   char mtext[128];    /* message data */
};

/*
 * Main method that will be executed when the process is run.
 * Holds logic for receiving messages from process 1.
 */
int main() {
    printf("this is process 2\n");
    int msqid;
	struct msgbuf ibuf;

	if ((msqid = msgget(MSG_KEY, IPC_CREAT | 0666)) < 0) {
		perror("initializing message in child1");
        return -1;
	}

    while(1) {
        if (msgrcv(msqid, &ibuf, 128, 1, 0) < 0) {
            perror("Error receiving message");
            return -1;
        }

        printf("Message received: %s\n", ibuf.mtext);
        fflush(stdout);
    }

    return 0;
}
