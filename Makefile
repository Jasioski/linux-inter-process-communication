all: process1.o process2.o
	gcc process1.o -o process1
	gcc process2.o -o process2

process1.o: process1.c ipcKeys.h
	gcc -c process1.c

process2.o: process2.c ipcKeys.h
	gcc -c process2.c

tags:
	ctags -R -V *

clean:
	rm -rf process2 process1 *.o *.gch
